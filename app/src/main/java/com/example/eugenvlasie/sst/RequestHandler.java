package com.example.eugenvlasie.sst;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.net.ssl.HttpsURLConnection;

import static android.content.Context.MODE_PRIVATE;
import static com.example.eugenvlasie.sst.MainActivity.token;
import static com.example.eugenvlasie.sst.SplashScreen.canClick;
import static com.example.eugenvlasie.sst.Synchronize.Sync;
import static com.example.eugenvlasie.sst.Synchronize.progressBar;
import static com.example.eugenvlasie.sst.Synchronize.tipConn;
import static com.example.eugenvlasie.sst.SplashScreen.timestamp;

class RequestHandler extends AsyncTask<Pair, Void, ArrayList<Pair<Integer, JSONObject>>> {
    private String ActivityName;
    private Activity activity;
    private ArrayList<String> LoginData = new ArrayList<>();
    private ProductDatabaseHandler productDatabase;
    private ProductDatabaseHandler localProductDatabase;
    static String shopId = "1";
    private boolean finishedCopying;
    private Boolean userIsClient = false;

    boolean isFinishedCopying() {
        return finishedCopying;
    }

    RequestHandler(Activity activity, String ActivityName) {
        this.activity = activity;
        this.ActivityName = ActivityName;

        finishedCopying = false;
    }

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {

        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            String Value = entry.getValue();
            LoginData.add(Value);
            result.append(URLEncoder.encode(Value, "UTF-8"));
        }

        return result.toString();

    }

    @Override
    protected ArrayList<Pair<Integer, JSONObject>> doInBackground(Pair... ConnData) {

        localProductDatabase = new ProductDatabaseHandler(activity, "AuxiliaryProductDatabase");
        productDatabase = new ProductDatabaseHandler(activity, "ProductDatabase");

        //ArrayList<Product> products = productDatabase.getProducts();

        //localProductDatabase.onUpgrade(localProductDatabase.getWritableDatabase(), 1, 1);
        //for (Product productIterator : products) {
            //localProductDatabase.addProduct(productIterator);
        //}

        finishedCopying = true;

        ArrayList<Pair<Integer, JSONObject>> responses;
        responses = new ArrayList<>();
        String response, response2;
        int responseCode;
        HttpURLConnection conn;
        URL url;
        HashMap<String, String> ConnParams;
        for (Pair ConnDetails : ConnData) {

            url = (URL) ConnDetails.getLeft();
            ConnParams = (HashMap<String, String>) ConnDetails.getRight();
            response = "";

            if (ActivityName.equals("Synchronize")) {

                try {

                    SharedPreferences prefs = activity.getSharedPreferences("timestamp", MODE_PRIVATE);
                    timestamp=prefs.getLong("seconds",0);
                    SQLiteDatabase Database = productDatabase.getWritableDatabase();
                    //productDatabase.onUpgrade(Database, 1, 1);
                    JSONObject paginator;
                    JSONArray Products;
                    int last_page = 1;
                    URL next_page_url = new URL(activity.getResources().getString(R.string.GetProductsURL));
                    HttpURLConnection conn2;
                    ImageHandler imageHandler;
                    String urlString;
                    int i, responseCode2;
                    url = new URL(activity.getResources().getString(R.string.LoginURL));
                    AuthenticationDatabaseHandler LoginDatabase = new AuthenticationDatabaseHandler(activity);

                    try {

                        conn = (HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("POST");
                        conn.setDoInput(true);
                        conn.setDoOutput(true);
                        conn.setConnectTimeout(30000);
                        conn.setReadTimeout(30000);

                        User user = LoginDatabase.getAllUsers().get(0);

                        ConnParams.clear();
                        ConnParams.put("email", user.getUsername());
                        ConnParams.put("password", user.getPassword());

                        OutputStream os = conn.getOutputStream();
                        BufferedWriter writer = new BufferedWriter(
                                new OutputStreamWriter(os, "UTF-8"));
                        writer.write(getPostDataString(ConnParams));
                        writer.flush();
                        writer.close();
                        os.close();
                        responseCode = conn.getResponseCode();

                        if (responseCode == HttpsURLConnection.HTTP_OK) {
                            String line;
                            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                            while ((line = br.readLine()) != null) {
                                response += line;
                            }
                        } else {
                            response = "{\"status\":\"error\",\"message\":error}";
                        }
                        JSONObject JSONResponse = new JSONObject(response);
                        token = JSONResponse.getString("token");

                        LoginDatabase.deleteUser(user);
                        user.setToken(token);
                        LoginDatabase.addUser(user);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    for (i = 1; i <= last_page && !isCancelled(); i++) {
                        next_page_url=new URL("http://api.justnow.co/api/v1/products?page=" + i +"&timestamp="+timestamp);
                        conn2 = (HttpURLConnection) next_page_url.openConnection();
                        conn2.setUseCaches(false);
                        conn2.setAllowUserInteraction(false);
                        conn2.addRequestProperty("Authorization", "Bearer " + token);

                        responseCode2 = conn2.getResponseCode();

                        if (responseCode2 == HttpsURLConnection.HTTP_OK) {
                            String line2;
                            response2 = "";
                            BufferedReader br2 = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
                            while ((line2 = br2.readLine()) != null) {
                                response2 += line2;
                            }
                        } else {
                            response2 = "{\"status\":\"error\",\"message\":error}";
                        }

                        JSONObject JSONResponse2 = new JSONObject(response2);
                        paginator = JSONResponse2.getJSONObject("paginator");
                        Products = JSONResponse2.getJSONArray("data");

                        last_page = paginator.getInt("last_page");

                        if (i < last_page)
                            next_page_url = new URL(paginator.getString("next_page_url"));
                        productDatabase.addProducts(Products);
                    }
                    long unixTime = System.currentTimeMillis() / 1000L;
                    SharedPreferences.Editor editor = activity.getSharedPreferences("timestamp", MODE_PRIVATE).edit();
                    editor.putLong("seconds", unixTime);
                    editor.apply();
                    timestamp=unixTime;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                responses.add(new Pair<>(200, new JSONObject()));
                return responses;
            } else {
                try {
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setConnectTimeout(30000);
                    conn.setReadTimeout(30000);

                    OutputStream os = conn.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(getPostDataString(ConnParams));
                    writer.flush();
                    writer.close();
                    os.close();
                    responseCode = conn.getResponseCode();

                    JSONObject JSONResponse;

                    if (responseCode == HttpsURLConnection.HTTP_OK) {
                        String line;
                        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        while ((line = br.readLine()) != null) {
                            response += line;
                        }
                        JSONResponse=new JSONObject(response);
                        if (JSONResponse.getJSONObject("data").getString("role").equals("Client"))
                            userIsClient=true;

                    } else {
                        response = "{\"status\":\"error\",\"message\":error}";
                        JSONResponse=new JSONObject(response);
                    }
                    responses.add(new Pair<>(responseCode, JSONResponse));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return responses;
    }

    /*private void mergeDatabases() {
        ArrayList<Product> localProducts, serverProducts;
        localProducts = localProductDatabase.getProducts();
        serverProducts = productDatabase.getProducts();
        String dateI;
        HashMap<Product, Integer> flags = new HashMap<>();
        Integer timezone;

        for (Product k : serverProducts) {
            flags.put(k, 0);
        }

        for (Product i : localProducts) {
            boolean found = false;
            for (Product j : serverProducts) {
                if (i.getBarcode().equals(j.getBarcode())) {
                    flags.put(j, 1);
                    found = true;
                    dateI = i.getUpdated_at().getDate();

                    if (i.getUpdated_at().getTimezone().length() > 3) {
                        timezone = Integer.valueOf(i.getUpdated_at().getTimezone().substring(3));
                        int hour = Integer.parseInt(dateI.substring(11, 13));
                        hour -= timezone;

                        int day = Integer.parseInt(dateI.substring(8, 10));
                        int month = Integer.parseInt(dateI.substring(5, 7));
                        int year = Integer.parseInt(dateI.substring(0, 4));

                        int min = Integer.parseInt(dateI.substring(14, 16));

                        if (hour < 1) {

                            day--;
                            hour = 24 + hour;

                            if (day < 1) {

                                Calendar calendar = new GregorianCalendar(year, month - 1, 1);
                                day = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                                month--;

                                if (month < 1) {
                                    year--;
                                }
                            }

                        } else if (hour > 24 || (hour == 24 && min != 0)) {

                            hour = hour - 24;
                            Calendar calendar = new GregorianCalendar(year, month - 1, 1);
                            int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

                            if (day + 1 > daysInMonth) {
                                day = 1;
                                month++;

                                if (month > 12) {
                                    month = 1;
                                    year++;
                                }
                            }

                        }
                        if (month < 10) {

                            if (day < 10) {

                                if (hour < 10) {
                                    dateI = year + "-0" + month + "-0" + day + " 0" + hour + dateI.substring(13);
                                } else {
                                    dateI = year + "-0" + month + "-0" + day + " " + hour + dateI.substring(13);
                                }
                            } else {
                                if (hour < 10) {
                                    dateI = year + "-0" + month + "-" + day + " 0" + hour + dateI.substring(13);
                                } else {
                                    dateI = year + "-0" + month + "-" + day + " " + hour + dateI.substring(13);
                                }
                            }
                        } else {
                            if (day < 10) {

                                if (hour < 10) {
                                    dateI = year + "-" + month + "-0" + day + " 0" + hour + dateI.substring(13);
                                } else {
                                    dateI = year + "-" + month + "-0" + day + " " + hour + dateI.substring(13);
                                }
                            } else {
                                if (hour < 10) {
                                    dateI = year + "-" + month + "-" + day + " 0" + hour + dateI.substring(13);
                                } else {
                                    dateI = year + "-" + month + "-" + day + " " + hour + dateI.substring(13);
                                }
                            }
                        }

                    }

                    Timestamp iUpdated_at = i.getUpdated_at();
                    Timestamp jUpdated_at = j.getUpdated_at();

                    iUpdated_at.setDate(dateI);
                    iUpdated_at.setTimezone("UTC");
                    if (!iUpdated_at.earlierThan(jUpdated_at)) {

                        productDatabase.deleteProduct(j);

                        Stock stock = i.getStock().get(i.getStock().size() - 1);

                        ArrayList<Stock> stocks = j.getStock();
                        stocks.add(stock);
                        j.setStock(stocks);
                        j.setUpdated_at(i.getUpdated_at());
                        j.setCreated_at(i.getCreated_at());
                        j.setBarcode(i.getBarcode());
                        j.setPrice(i.getPrice());
                        j.setProduct_image(i.getProduct_image());
                        j.setProduct_name(i.getProduct_name());
                        j.setShort_description(i.getShort_description());
                        j.setSku(i.getSku());
                        j.setShop_id(i.getShop_id());
                        j.setId(i.getId());
                        j.setServerId(i.getServerId());
                        i.setStock(j.getStock());

                        //productDatabase.addProduct(j);
                        updateProductToServer(j);
                    } else {

                        File image = new File(activity.getFilesDir() + "/" + j.getBarcode() + "(s).jpg");
                        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);

                        ImageHandler imageHandler = new ImageHandler(j.getBarcode() + ".jpg", bitmap, activity);

                        try {
                            imageHandler.saveImageToInternalStorage();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        activity.deleteFile(j.getBarcode() + "(s).jpg");
                    }
                }
            }
            if (!found) {
                addProductToServer(i);
                //productDatabase.addProduct(i);
            }
        }

        Set<Map.Entry<Product, Integer>> set = flags.entrySet();

        for (Object aSet : set) {
            Map.Entry entry = (Map.Entry) aSet;

            if (entry.getValue().equals(0)) {

                Product temp = (Product) entry.getKey();

                File image = new File(activity.getFilesDir() + "/" + temp.getBarcode() + "(s).jpg");
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);

                ImageHandler imageHandler = new ImageHandler(temp.getBarcode() + ".jpg", bitmap, activity);

                try {
                    imageHandler.saveImageToInternalStorage();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                activity.deleteFile(temp.getBarcode() + "(s).jpg");
            }
        }
    }*/

    private void addProductToServer(Product product) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://justnow.stonesoupdev.com/api/v1/products");
        try {
            FileBody bin = new FileBody(new File(activity.getFilesDir() + "/" + product.getBarcode() + ".jpg"));

            MultipartEntity reqEntity = new MultipartEntity();
            reqEntity.addPart("image", bin);
            reqEntity.addPart("name", new StringBody(product.getProduct_name()));
            reqEntity.addPart("price", new StringBody(product.getPrice()));
            reqEntity.addPart("description", new StringBody(product.getShort_description()));
            reqEntity.addPart("shop_id", new StringBody(product.getShop_id()));
            reqEntity.addPart("barcode", new StringBody(product.getBarcode()));
            reqEntity.addPart("sku", new StringBody(product.getSku()));

            ArrayList<Stock> stocks = product.getStock();
            int stockCounter = 0;

            for (Stock stockIterator : stocks) {
                reqEntity.addPart("stock[" + String.valueOf(stockCounter) + "][units]", new StringBody(stockIterator.getUnits()));
                reqEntity.addPart("stock[" + String.valueOf(stockCounter) + "][expiration_date]", new StringBody(stockIterator.getExpiration_date()));
                stockCounter++;
            }

            httppost.setHeader("Authorization", "Bearer " + token);
            httppost.setEntity(reqEntity);


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpclient.getConnectionManager().shutdown();
        }
    }

    private void updateProductToServer(Product product) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://justnow.stonesoupdev.com/api/v1/products/" + product.getServerId());
        try {
            FileBody bin = new FileBody(new File(activity.getFilesDir() + "/" + product.getBarcode() + ".jpg"));

            MultipartEntity reqEntity = new MultipartEntity();
            reqEntity.addPart("image", bin);

            reqEntity.addPart("name", new StringBody(product.getProduct_name()));
            reqEntity.addPart("price", new StringBody(product.getPrice()));
            reqEntity.addPart("description", new StringBody(product.getShort_description()));
            reqEntity.addPart("shop_id", new StringBody(product.getShop_id()));
            reqEntity.addPart("barcode", new StringBody(product.getBarcode()));
            reqEntity.addPart("sku", new StringBody(product.getSku()));

            ArrayList<Stock> stocks = product.getStock();
            int stockCounter = 0;

            for (Stock stockIterator : stocks) {
                reqEntity.addPart("stock[" + String.valueOf(stockCounter) + "][units]", new StringBody(stockIterator.getUnits()));
                reqEntity.addPart("stock[" + String.valueOf(stockCounter) + "][expiration_date]", new StringBody(stockIterator.getExpiration_date()));
                stockCounter++;
            }

            httppost.setHeader("Authorization", "Bearer " + token);
            httppost.setEntity(reqEntity);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpclient.getConnectionManager().shutdown();
        }
    }


    @Override
    protected void onPostExecute(ArrayList<Pair<Integer, JSONObject>> responses) {
        if (responses.size() == 1) {
            Pair<Integer, JSONObject> response = responses.get(0);
            if (response.getLeft() == HttpsURLConnection.HTTP_OK) {
                switch (ActivityName) {
                    case "MainActivity":
                        if(userIsClient)
                        {
                            AlertDialog.Builder OKdlgAlert = new AlertDialog.Builder(activity);
                            OKdlgAlert.setTitle("Error");
                            OKdlgAlert.setOnCancelListener(new DialogInterface.OnCancelListener()
                            {

                                @Override
                                public void onCancel(DialogInterface dialogInterface) {
                                    canClick=true;
                                }
                            });
                            OKdlgAlert.setMessage("This account appears to be a client account. Try again with a merchandiser account!");
                            OKdlgAlert.setPositiveButton("OK", null);
                            OKdlgAlert.setCancelable(true);
                            OKdlgAlert.create().show();
                            canClick=true;
                        }
                        else {
                            AuthenticationDatabaseHandler LoginDatabase = new AuthenticationDatabaseHandler(activity);
                            try {
                                token = response.getRight().getString("token");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (LoginDatabase.getUserCount() == 0) {
                                User user = new User(1, LoginData.get(0), LoginData.get(1), token);
                                LoginDatabase.addUser(user);
                            }
                            activity.finish();
                            activity.startActivity(new Intent(activity, Dashboard.class));
                        }
                        break;
                    case "ForgotPassword":
                        AlertDialog.Builder OKdlgAlert = new AlertDialog.Builder(activity);
                        OKdlgAlert.setTitle("Congratulation!");
                        OKdlgAlert.setMessage("A password reset request has been sent. Please verify your email, check also the SPAM/Junk folder.");
                        OKdlgAlert.setPositiveButton("OK", null);
                        OKdlgAlert.setCancelable(true);
                        OKdlgAlert.create().show();
                        break;
                    case "Synchronize":
                        if (isNetworkAvailable()) {

                            /*final Handler handler = new Handler() {
                                @Override
                                public void handleMessage(Message msg) {*/

                                    //Log.e("pd stopped","yep");
                                    tipConn.setText(activity.getResources().getString(R.string.sync_success));
                                    progressBar.setVisibility(View.GONE);
                                    Sync.setVisibility(View.VISIBLE);
                                }
                            /*};

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    ManageProducts.products = productDatabase.getProducts();
                                    handler.sendEmptyMessage(0);
                                    //Log.e("in thread","yep");
                                }
                            }).start();



                        }*/
                        break;
                    default:
                        AlertDialog.Builder ErrorDlgAlert = new AlertDialog.Builder(activity);
                        ErrorDlgAlert.setMessage("Error");
                        ErrorDlgAlert.setPositiveButton("OK", null);
                        ErrorDlgAlert.setTitle("Error!");
                        ErrorDlgAlert.setCancelable(true);
                        ErrorDlgAlert.create().show();
                        break;
                }
            } else {
                switch (ActivityName) {
                    case "MainActivity":
                        AlertDialog.Builder MaindlgAlert = new AlertDialog.Builder(activity);
                        MaindlgAlert.setMessage("The username and password you have entered are not valid.");
                        MaindlgAlert.setTitle("Error!");
                        MaindlgAlert.setOnCancelListener(new DialogInterface.OnCancelListener()
                        {

                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                canClick=true;
                            }
                        });
                        MaindlgAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                canClick = true;
                            }
                        });
                        MaindlgAlert.setCancelable(true);
                        MaindlgAlert.create().show();
                        break;
                    case "ForgotPassword":
                        AlertDialog.Builder ForgotPasswordDlgAlert = new AlertDialog.Builder(activity);
                        ForgotPasswordDlgAlert.setMessage("The email address you have entered is not valid. Please enter a valid email address.");
                        ForgotPasswordDlgAlert.setTitle("Error!");
                        ForgotPasswordDlgAlert.setPositiveButton("OK", null);
                        ForgotPasswordDlgAlert.setCancelable(true);
                        ForgotPasswordDlgAlert.create().show();
                        break;
                    case "Synchronize":
                        break;
                    default:
                        AlertDialog.Builder ErrorDlgAlert = new AlertDialog.Builder(activity);
                        ErrorDlgAlert.setMessage("Error");
                        ErrorDlgAlert.setPositiveButton("OK", null);
                        ErrorDlgAlert.setTitle("Error!");
                        ErrorDlgAlert.setCancelable(true);
                        ErrorDlgAlert.create().show();
                        break;
                }
            }
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
