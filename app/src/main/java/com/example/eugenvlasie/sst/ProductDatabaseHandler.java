package com.example.eugenvlasie.sst;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.eugenvlasie.sst.RequestHandler.shopId;


class ProductDatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 9;
    private final static String TABLE_PRODUCTS = "Products";
    private final static String TABLE_STOCKS = "Stocks";
    private static final String KEY_ID = "id";
    private static final String SERVER_ID = "server_id";
    private static final String PRODUCT_NAME = "product_name";
    private static final String SKU = "sku";
    private static final String PRODUCT_IMAGE = "product_image";
    private static final String SHORT_DESCRIPTION = "short_description";
    private static final String PRICE = "price";
    private static final String SHOP_ID = "shop_id";
    private static final String BARCODE = "barcode";
    private static final String UNITS = "units";
    private static final String CHANGED = "changed";
    private static final String EXPIRATION_DATE = "expiration_date";
    private static final String CREATED_AT = "created_at";
    private static final String UPDATED_AT = "updated_at";
    private static final String CREATED_AT_DATE = "created_at_date";
    private static final String CREATED_AT_TIMEZONE_TYPE = "created_at_timezone_type";
    private static final String CREATED_AT_TIMEZONE = "created_at_timezone";
    private static final String UPDATED_AT_DATE = "updated_at_date";
    private static final String UPDATED_AT_TIMEZONE_TYPE = "updated_at_timezone_type";
    private static final String UPDATED_AT_TIMEZONE = "updated_at_timezone";
    private static final String PRODUCT_ID = "product_barcode";

    ProductDatabaseHandler(Context context, String DATABASE_NAME) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_PRODUCTS_TABLE = "CREATE TABLE " + TABLE_PRODUCTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + PRODUCT_NAME + " TEXT," + CHANGED + " INTEGER," +
                SKU + " TEXT," + BARCODE + " TEXT," + PRICE + " TEXT, " + PRODUCT_IMAGE
                + " TEXT," + SHORT_DESCRIPTION + " TEXT," + SHOP_ID +
                " TEXT," + CREATED_AT_DATE + " TEXT," + CREATED_AT_TIMEZONE_TYPE + " TEXT,"
                + CREATED_AT_TIMEZONE + " TEXT," + UPDATED_AT_DATE + " TEXT," + UPDATED_AT_TIMEZONE_TYPE
                + " TEXT," + UPDATED_AT_TIMEZONE + " TEXT," + SERVER_ID + " TEXT)";
        String CREATE_STOCK_TABLE = "CREATE TABLE " + TABLE_STOCKS + " (" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + UNITS +
                " TEXT," + PRODUCT_ID + " TEXT," + EXPIRATION_DATE + " TEXT," + CREATED_AT + " TEXT," + UPDATED_AT + " TEXT)";
        sqLiteDatabase.execSQL(CREATE_PRODUCTS_TABLE);
        sqLiteDatabase.execSQL(CREATE_STOCK_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_STOCKS);
        onCreate(sqLiteDatabase);
    }

    private void addStock(JSONArray stocks, String barcode) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();

        StringBuilder INSERT_ALL_STOCKS = new StringBuilder("INSERT INTO " + TABLE_STOCKS + "(" + UNITS + ", " + PRODUCT_ID + ", " +
                EXPIRATION_DATE + ", " + CREATED_AT + ", " + UPDATED_AT + ") VALUES ");
        int size_of_stocks = stocks.length();
        JSONObject stock;
        /*for (i = 0; i < size_of_stocks - 1; i++) {
            if(i==0) {
                stock = stocks.getJSONObject(i);
                INSERT_ALL_STOCKS.append("(");//.append(DatabaseUtils.sqlEscapeString(barcode)).append(", ");
                INSERT_ALL_STOCKS.append(DatabaseUtils.sqlEscapeString(stock.getString("units"))).append(", ");
                INSERT_ALL_STOCKS.append(barcode).append(", ");
                INSERT_ALL_STOCKS.append(DatabaseUtils.sqlEscapeString(stock.getString("expiration_date"))).append(", ");
                INSERT_ALL_STOCKS.append(DatabaseUtils.sqlEscapeString(stock.getString("created_at"))).append(", ");
                INSERT_ALL_STOCKS.append(DatabaseUtils.sqlEscapeString(stock.getString("updated_at"))).append("), ");
            }
        }
        if (i > 0) {
            stock = stocks.getJSONObject(i);
            INSERT_ALL_STOCKS.append("(");//.append(DatabaseUtils.sqlEscapeString(barcode)).append(", ");
            INSERT_ALL_STOCKS.append(DatabaseUtils.sqlEscapeString(stock.getString("units"))).append(", ");
            INSERT_ALL_STOCKS.append(barcode).append(", ");
            INSERT_ALL_STOCKS.append(DatabaseUtils.sqlEscapeString(stock.getString("expiration_date"))).append(", ");
            INSERT_ALL_STOCKS.append(DatabaseUtils.sqlEscapeString(stock.getString("created_at"))).append(", ");
            INSERT_ALL_STOCKS.append(DatabaseUtils.sqlEscapeString(stock.getString("updated_at"))).append(");");
            }*/
        if (size_of_stocks > 0) {
            stock = stocks.getJSONObject(size_of_stocks - 1);
            INSERT_ALL_STOCKS.append("(");//.append(DatabaseUtils.sqlEscapeString(barcode)).append(", ");
            INSERT_ALL_STOCKS.append(DatabaseUtils.sqlEscapeString(stock.getString("units"))).append(", ");
            INSERT_ALL_STOCKS.append(barcode).append(", ");
            INSERT_ALL_STOCKS.append(DatabaseUtils.sqlEscapeString(stock.getString("expiration_date"))).append(", ");
            INSERT_ALL_STOCKS.append(DatabaseUtils.sqlEscapeString(stock.getString("created_at"))).append(", ");
            INSERT_ALL_STOCKS.append(DatabaseUtils.sqlEscapeString(stock.getString("updated_at"))).append(");");
            db.execSQL(INSERT_ALL_STOCKS.toString());
        }
    }

    void addProducts(JSONArray products) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();

        StringBuilder INSERT_ALL_PRODUCTS = new StringBuilder("INSERT INTO " + TABLE_PRODUCTS + "(" + PRODUCT_NAME + ", " + CHANGED + ", " +
                SKU + ", " + BARCODE + ", " + PRICE + ", " + PRODUCT_IMAGE
                + ", " + SHORT_DESCRIPTION + ", " + SHOP_ID +
                ", " + CREATED_AT_DATE + ", " + CREATED_AT_TIMEZONE_TYPE + ", "
                + CREATED_AT_TIMEZONE + ", " + UPDATED_AT_DATE + ", " + UPDATED_AT_TIMEZONE_TYPE
                + ", " + UPDATED_AT_TIMEZONE + ") VALUES ");

        int size_of_products = products.length();
        JSONObject product;
        int i;
        for (int j = 0; j <= 9 && j * 100 < size_of_products; j++) {
            for (i = 0; i + j * 100 < size_of_products - 1 && i < 99; i++) {
                product = products.getJSONObject(i + j * 100);
                INSERT_ALL_PRODUCTS.append("(").append(DatabaseUtils.sqlEscapeString(product.getString("name"))).append(", ");
                if(product.getString("name").equals("500G FLORA X/LITE")) {
                    Log.e("found", product.getString("name"));
                }
                INSERT_ALL_PRODUCTS.append(0).append(", ");
                INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(product.getString("sku"))).append(", ");
                INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(product.getString("barcode"))).append(", ");
                INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(product.getString("price"))).append(", ");
                INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(product.getString("image"))).append(", ");
                INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(product.getString("description"))).append(", ");
                INSERT_ALL_PRODUCTS.append(shopId).append(", ");
                JSONObject Created_at = product.getJSONObject("created_at");
                JSONObject Updated_at = product.getJSONObject("updated_at");
                INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(Created_at.getString("date"))).append(", ");
                INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(Created_at.getString("timezone_type"))).append(", ");
                INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(Created_at.getString("timezone"))).append(", ");
                INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(Updated_at.getString("date"))).append(", ");
                INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(Updated_at.getString("timezone_type"))).append(", ");
                INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(Updated_at.getString("timezone"))).append("), ");

                /*String barcode = DatabaseUtils.sqlEscapeString(product.getString("barcode"));
                Log.e("barcode",barcode+" s");
                String barcode2 = product.getString("barcode");
                Log.e("barcode2,",barcode+" s2");*/
                //String tempBarcode = DatabaseUtils.sqlEscapeString(product.getString("barcode"));
                //tempBarcode = tempBarcode.substring(1,tempBarcode.length()-1);
                //Log.e("barcode",tempBarcode+" s");
                
                //addStock(product.getJSONArray("stock"), tempBarcode);
                //Log.d("number entrance "+j,i+"");
            }

            product = products.getJSONObject(i + j * 100);
            INSERT_ALL_PRODUCTS.append("(").append(DatabaseUtils.sqlEscapeString(product.getString("name"))).append(", ");
            INSERT_ALL_PRODUCTS.append(0).append(", ");
            INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(product.getString("sku"))).append(", ");
            INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(product.getString("barcode"))).append(", ");
            INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(product.getString("price"))).append(", ");
            INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(product.getString("image"))).append(", ");
            INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(product.getString("description"))).append(", ");
            INSERT_ALL_PRODUCTS.append(shopId).append(", ");
            JSONObject Created_at = product.getJSONObject("created_at");
            JSONObject Updated_at = product.getJSONObject("updated_at");
            INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(Created_at.getString("date"))).append(", ");
            INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(Created_at.getString("timezone_type"))).append(", ");
            INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(Created_at.getString("timezone"))).append(", ");
            INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(Updated_at.getString("date"))).append(", ");
            INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(Updated_at.getString("timezone_type"))).append(", ");
            INSERT_ALL_PRODUCTS.append(DatabaseUtils.sqlEscapeString(Updated_at.getString("timezone"))).append(");");
            addStock(product.getJSONArray("stock"), product.getString("barcode"));
            Log.e("request", INSERT_ALL_PRODUCTS.toString());
            db.execSQL(INSERT_ALL_PRODUCTS.toString());
            INSERT_ALL_PRODUCTS = new StringBuilder("INSERT INTO " + TABLE_PRODUCTS + "(" + PRODUCT_NAME + ", " + CHANGED + ", " +
                    SKU + ", " + BARCODE + ", " + PRICE + ", " + PRODUCT_IMAGE
                    + ", " + SHORT_DESCRIPTION + ", " + SHOP_ID +
                    ", " + CREATED_AT_DATE + ", " + CREATED_AT_TIMEZONE_TYPE + ", "
                    + CREATED_AT_TIMEZONE + ", " + UPDATED_AT_DATE + ", " + UPDATED_AT_TIMEZONE_TYPE
                    + ", " + UPDATED_AT_TIMEZONE + ") VALUES ");
        }
        //Log.e("Length", String.valueOf(getProducts().size()));

    }

    void addProduct(Product product,int changed) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues productValues = new ContentValues();
        productValues.put(PRODUCT_NAME, product.getProduct_name());
        productValues.put(CHANGED, changed);
        productValues.put(SKU, product.getSku());
        productValues.put(BARCODE, product.getBarcode());
        productValues.put(PRICE, product.getPrice());
        productValues.put(PRODUCT_IMAGE, product.getProduct_image());
        productValues.put(SHORT_DESCRIPTION, product.getShort_description());
        productValues.put(SHOP_ID, product.getShop_id());
        productValues.put(CREATED_AT_DATE, product.getCreated_at().getDate());
        productValues.put(CREATED_AT_TIMEZONE_TYPE, product.getCreated_at().getTimezone_type());
        productValues.put(CREATED_AT_TIMEZONE, product.getCreated_at().getTimezone());
        productValues.put(UPDATED_AT_DATE, product.getUpdated_at().getDate());
        productValues.put(UPDATED_AT_TIMEZONE_TYPE, product.getUpdated_at().getTimezone_type());
        productValues.put(UPDATED_AT_TIMEZONE, product.getUpdated_at().getTimezone());
        productValues.put(SERVER_ID, product.getServerId());
        ArrayList<Stock> stocks = product.getStock();
        for (Stock stockIterator : stocks) {

            ContentValues stockValues = new ContentValues();

            stockValues.put(UNITS, stockIterator.getUnits());
            stockValues.put(PRODUCT_ID,product.getBarcode());
            stockValues.put(EXPIRATION_DATE, stockIterator.getExpiration_date());
            stockValues.put(CREATED_AT, stockIterator.getCreated_at());
            stockValues.put(UPDATED_AT, stockIterator.getUpdated_at());

            db.insert(TABLE_STOCKS, null, stockValues);
        }
        db.insert(TABLE_PRODUCTS, null, productValues);
        db.close();
        Log.e("reached","reached");
    }


    ArrayList<Product> getProducts(int changed) {
        ArrayList<Product> products = new ArrayList<>();


        //String selectProductQuery = "SELECT * FROM " + TABLE_PRODUCTS ;

        SQLiteDatabase db = this.getWritableDatabase();
        int k;
        String selectProductQuery;
        Cursor productsCursor;
        Cursor stockCursor;
        for (int pag = 0; ; pag += 100) {
            k=0;
            if(changed==0)
                selectProductQuery = "SELECT * FROM " + TABLE_PRODUCTS + " LIMIT " + pag + ", 100"; //Mai intai treci de la cat sa plece, apoi cate sa ia
            else
                selectProductQuery = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE " + CHANGED + " =1 LIMIT " + pag + ", 100";
            /*if(productsCursor!=null&&!productsCursor.isClosed()) {
                Log.e("closing product","reached");
                productsCursor.close();
            }*/
            productsCursor = db.rawQuery(selectProductQuery, null);
            try {
                if (productsCursor.moveToFirst()) {
                    do {
                        k++;
                        Product product = new Product();
                        product.setId(Integer.parseInt(productsCursor.getString(0)));
                        product.setProduct_name(productsCursor.getString(1));
                        product.setSku(productsCursor.getString(2));
                        product.setBarcode(productsCursor.getString(3));
                        product.setPrice(productsCursor.getString(4));
                        product.setProduct_image(productsCursor.getString(5));
                        product.setShort_description(productsCursor.getString(6));
                        product.setShop_id(productsCursor.getString(7));
                        product.setCreated_at(new Timestamp(productsCursor.getString(8), productsCursor.getString(9), productsCursor.getString(10)));
                        product.setUpdated_at(new Timestamp(productsCursor.getString(11), productsCursor.getString(12), productsCursor.getString(13)));
                        product.setServerId(productsCursor.getString(14));
                        String selectStockQuery = "SELECT * FROM " + TABLE_STOCKS;

                        /*if(stockCursor!=null&&!stockCursor.isClosed()) {
                            Log.e("closing stock","reached");
                            stockCursor.close();
                        }*/

                        stockCursor = db.rawQuery(selectStockQuery, null);

                        ArrayList<Stock> stocks = new ArrayList<>();

                        if (stockCursor.moveToFirst()) {
                            do {
                                if (stockCursor.getString(stockCursor.getColumnIndex(PRODUCT_ID)).equals(product.getBarcode())) {
                                    //Log.d("added stock",product.getBarcode());
                                    Stock stock = new Stock();
                                    stock.setId(Integer.parseInt(stockCursor.getString(0)));
                                    stock.setUnits(stockCursor.getString(1));
                                    stock.setExpiration_date(stockCursor.getString(3));
                                    stock.setCreated_at(stockCursor.getString(4));
                                    stock.setUpdated_at(stockCursor.getString(5));
                                    stocks.add(stock);
                                }
                            } while (stockCursor.moveToNext());
                            stockCursor.close();
                        }
                        product.setStock(stocks);
                        products.add(product);
                        //product.printProduct();
                        stockCursor.close();
                    } while (productsCursor.moveToNext()&&k<=100);
                    productsCursor.close();
                    if (k < 100)
                        break;
                }
                else
                {
                    break;
                }
                productsCursor.close();
            }catch (java.lang.RuntimeException e)
            {
                Log.e("exception",e.toString());
            }
        }

        //

        /*selectProductQuery = "SELECT * FROM " + TABLE_PRODUCTS + " LIMIT 100, 101";
        //String selectProductQuery = "SELECT * FROM " + TABLE_PRODUCTS ;

        productsCursor = db.rawQuery(selectProductQuery, null);

        if (productsCursor.moveToFirst()) {
            do {
                Product product = new Product();
                product.setId(Integer.parseInt(productsCursor.getString(0)));
                product.setProduct_name(productsCursor.getString(1));
                product.setSku(productsCursor.getString(2));
                product.setBarcode(productsCursor.getString(3));
                product.setPrice(productsCursor.getString(4));
                product.setProduct_image(productsCursor.getString(5));
                product.setShort_description(productsCursor.getString(6));
                product.setShop_id(productsCursor.getString(7));
                product.setCreated_at(new Timestamp(productsCursor.getString(8), productsCursor.getString(9), productsCursor.getString(10)));
                product.setUpdated_at(new Timestamp(productsCursor.getString(11), productsCursor.getString(12), productsCursor.getString(13)));
                product.setServerId(productsCursor.getString(14));
                String selectStockQuery = "SELECT * FROM " + TABLE_STOCKS;

                Cursor stockCursor = db.rawQuery(selectStockQuery, null);

                ArrayList<Stock> stocks = new ArrayList<>();

                if (stockCursor.moveToFirst()) {
                    do {
                        if (stockCursor.getString(2).equals(product.getBarcode())) {
                            Stock stock = new Stock();
                            stock.setId(Integer.parseInt(stockCursor.getString(0)));
                            stock.setUnits(stockCursor.getString(1));
                            stock.setExpiration_date(stockCursor.getString(3));
                            stock.setCreated_at(stockCursor.getString(4));
                            stock.setUpdated_at(stockCursor.getString(5));
                            stocks.add(stock);
                        }
                    } while (stockCursor.moveToNext());
                }
                product.setStock(stocks);
                products.add(product);
                product.printProduct();
            } while (productsCursor.moveToNext());
        }

        /**/

        return products;
    }


    void deleteProduct(Product product) {
        SQLiteDatabase db = this.getWritableDatabase();
        /*db.execSQL("DROP TABLE IF EXISTS " + TABLE_STOCKS + product.getBarcode());*/

        long deletedRow = db.delete(TABLE_STOCKS, PRODUCT_ID + " = ?",
                new String[]{String.valueOf(product.getId())});

        Log.e("deletedRow Stock",deletedRow+"");

        long deletedProduct = db.delete(TABLE_PRODUCTS, BARCODE + " = ?",
                new String[]{String.valueOf(product.getBarcode())});

        Log.e("deletedProduct",deletedProduct+"");
        db.close();
    }

}
