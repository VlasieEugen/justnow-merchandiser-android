package com.example.eugenvlasie.sst;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import static com.example.eugenvlasie.sst.NewProduct.BARCODE_REQUEST;
import static com.example.eugenvlasie.sst.NewProduct.REQUEST_CAMERA;




public class Dashboard extends AppCompatActivity {


    private String[] mobileArray = {"Quick scan product", "Add new product", "Manage products", "Synchronize", "Sign out"};
    private int[] imageArray = {R.drawable.receipe, R.drawable.add, R.drawable.list, R.drawable.sync_small, R.drawable.logout};
    static String QR_BARCODE = "barcodeQRScan";

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar_logo);
        getSupportActionBar().setElevation(0);

        ImageView backButton = (ImageView) findViewById(R.id.back_button);
        backButton.setVisibility(View.GONE);

        setContentView(R.layout.dashboard);

        ListView Menu = (ListView) findViewById(R.id.dashboard_menu);
        Menu.setAdapter(new DashboardAdapter(this, mobileArray, imageArray));

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        switch (requestCode) {

            case REQUEST_CAMERA: {

                if (grantResults.length > 0) {

                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                        Intent qrIntent = new Intent(Dashboard.this, QRScanner.class);
                        qrIntent.putExtra("requestBarcode", BARCODE_REQUEST);
                        Dashboard.this.startActivityForResult(qrIntent, BARCODE_REQUEST);

                    } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {

                        if (ActivityCompat.shouldShowRequestPermissionRationale(Dashboard.this, Manifest.permission.CAMERA)) {

                            Toast.makeText(Dashboard.this, "This app requires this permission in order to acces your camera!", Toast.LENGTH_SHORT).show();

                        } else {

                            Toast.makeText(Dashboard.this, "You have checked the \"Don't ask again\" box!", Toast.LENGTH_SHORT).show();

                        }
                    }
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BARCODE_REQUEST) {

            if (resultCode == RESULT_OK) {

                String code = data.getStringExtra("barcode");
                Intent tempIntent = new Intent(Dashboard.this, NewProduct.class);
                tempIntent.putExtra(QR_BARCODE, code);
                Dashboard.this.startActivity(tempIntent);

            }
        }
    }
}
