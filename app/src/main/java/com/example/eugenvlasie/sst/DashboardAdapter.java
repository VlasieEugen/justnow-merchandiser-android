package com.example.eugenvlasie.sst;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import static com.example.eugenvlasie.sst.NewProduct.BARCODE_REQUEST;
import static com.example.eugenvlasie.sst.NewProduct.REQUEST_CAMERA;
import static com.example.eugenvlasie.sst.SplashScreen.canClick;

class DashboardAdapter extends BaseAdapter {

    private String[] result;
    private Context context;
    private int[] imageId;
    private Activity activity;
    private static LayoutInflater inflater = null;
    private ArrayList<User> users = new ArrayList<>();
    private AuthenticationDatabaseHandler db;


    DashboardAdapter(Activity mainActivity, String[] prgmNameList, int[] prgmImages) {

        activity = mainActivity;
        db = new AuthenticationDatabaseHandler(activity);
        users = db.getAllUsers();

        result = prgmNameList;
        context = mainActivity;
        imageId = prgmImages;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return result.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class Holder {

        TextView tv;
        ImageView img;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.menu_item, null);
        holder.tv = (TextView) rowView.findViewById(R.id.Menu_Item);
        holder.img = (ImageView) rowView.findViewById(R.id.menu_image);
        holder.tv.setText(result[position]);
        holder.img.setImageResource(imageId[position]);
        rowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (position) {

                    case 0: {

                        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);

                        } else {

                            Intent qrIntent = new Intent(activity, QRScanner.class);
                            qrIntent.putExtra("requestBarcode", BARCODE_REQUEST);
                            activity.startActivityForResult(qrIntent, BARCODE_REQUEST);

                        }

                    }

                    break;

                    case 1:

                        activity.startActivity(new Intent(activity, NewProduct.class));
                        break;

                    case 2:

                        activity.startActivity(new Intent(activity, ManageProducts.class));
                        break;

                    case 3:

                        activity.startActivity(new Intent(activity, Synchronize.class));
                        break;

                    case 4:

                        activity.finish();
                        canClick = true;
                        db.deleteUser(users.get(0));
                        activity.startActivity(new Intent(activity, MainActivity.class));
                        break;

                }

            }
        });

        return rowView;

    }

}