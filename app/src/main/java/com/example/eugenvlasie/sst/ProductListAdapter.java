package com.example.eugenvlasie.sst;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

class ProductListAdapter extends BaseAdapter implements Filterable {

    private Activity activity;
    private ArrayList<Product> productArrayList;
    private ArrayList<Product> orig;

    ProductListAdapter(Activity activity, ArrayList<Product> productArrayList) {
        super();
        this.activity = activity;
        this.productArrayList = productArrayList;
    }


    private class productHolder {
        TextView product_name;
        TextView barcode;
        ImageView imageView;
        ImageHandler imageHandler;
    }

    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<Product> results = new ArrayList<>();
                if (orig == null)
                    orig = productArrayList;
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (Product g : orig) {
                            if (g.getProduct_name().toLowerCase()
                                    .contains(constraint.toString()))
                                results.add(g);
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          Filter.FilterResults results) {
                productArrayList = (ArrayList<Product>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return productArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return productArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final productHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(activity).inflate(R.layout.product_menu, parent, false);
            holder = new productHolder();
            holder.product_name = (TextView) convertView.findViewById(R.id.productName);
            holder.barcode = (TextView) convertView.findViewById(R.id.Barcode);
            holder.imageView = (ImageView) convertView.findViewById(R.id.imageView);
            convertView.setTag(holder);
        } else {
            holder = (productHolder) convertView.getTag();
        }
        Product product = productArrayList.get(position);
        holder.product_name.setText(product.getProduct_name());
        String tempBarcode = "Product code: " + String.valueOf(product.getBarcode());
        holder.barcode.setText(tempBarcode);


        holder.imageHandler = new ImageHandler(product.getBarcode() + ".jpg", product.getProduct_image(), activity);
        /*try {
            holder.imageView.setImageBitmap(holder.imageHandler.getThumbnail());
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("No image","locally");
        }*/
        return convertView;
    }
}

