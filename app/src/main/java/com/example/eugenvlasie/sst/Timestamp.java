package com.example.eugenvlasie.sst;

class Timestamp {
    private String date;
    private String timezone_type;

    String getDate() {
        return date;
    }


    void setDate(String date) {
        this.date = date;
    }

    String getTimezone_type() {
        return timezone_type;
    }

    /*void setTimezone_type(String timezone_type) {
        this.timezone_type = timezone_type;
    }*/

    String getTimezone() {
        return timezone;
    }

    void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    Timestamp() {

    }

    Timestamp(String date, String timezone_type, String timezone) {

        this.date = date;
        this.timezone_type = timezone_type;
        this.timezone = timezone;
    }

    boolean earlierThan(Timestamp timestamp) {
        Integer numberThis, numberCompareWith;
        String dateThis, dateCompareWith;
        dateThis = this.date;
        dateCompareWith = timestamp.date;
        numberThis = Integer.valueOf(dateThis.substring(0, 4));
        numberCompareWith = Integer.valueOf(dateCompareWith.substring(0, 4));

        if (numberThis < numberCompareWith)
            return true;
        if (numberThis > numberCompareWith)
            return false;

        numberThis = Integer.valueOf(dateThis.substring(5, 7));
        numberCompareWith = Integer.valueOf(dateCompareWith.substring(5, 7));

        if (numberThis < numberCompareWith)
            return true;
        if (numberThis > numberCompareWith)
            return false;

        numberThis = Integer.valueOf(dateThis.substring(8, 10));
        numberCompareWith = Integer.valueOf(dateCompareWith.substring(8, 10));

        if (numberThis < numberCompareWith)
            return true;
        if (numberThis > numberCompareWith)
            return false;

        numberThis = Integer.valueOf(dateThis.substring(11, 13));
        numberCompareWith = Integer.valueOf(dateCompareWith.substring(11, 13));

        if (numberThis < numberCompareWith)
            return true;
        if (numberThis > numberCompareWith)
            return false;

        numberThis = Integer.valueOf(dateThis.substring(14, 16));
        numberCompareWith = Integer.valueOf(dateCompareWith.substring(14, 16));

        if (numberThis < numberCompareWith)
            return true;
        if (numberThis > numberCompareWith)
            return false;

        numberThis = Integer.valueOf(dateThis.substring(17, 19));
        numberCompareWith = Integer.valueOf(dateCompareWith.substring(17, 19));

        return numberThis < numberCompareWith || numberThis <= numberCompareWith;

    }
    private String timezone;
}
