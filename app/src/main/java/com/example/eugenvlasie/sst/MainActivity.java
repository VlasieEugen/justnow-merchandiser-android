package com.example.eugenvlasie.sst;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import static com.example.eugenvlasie.sst.SplashScreen.canClick;

public class MainActivity extends Activity {


    private CheckBox saveLoginCheckBox;
    private SharedPreferences.Editor loginPrefsEditor;
    private EditText user, pass;
    private String username, password;
    private Pair<URL, HashMap<String, String>> ConnData = null;
    private RequestHandler requestHandler;
    static String token;


    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        TextView forgotPassword;
        boolean saveLogin;
        final Button loginButton;
        SharedPreferences loginPreferences;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        loginButton = (Button) findViewById(R.id.loginButton);
        user = (EditText) findViewById(R.id.username);
        pass = (EditText) findViewById(R.id.password);
        forgotPassword = (TextView) findViewById(R.id.forgot_password);
        forgotPassword.setTextColor(Color.parseColor("#FF5900"));
        saveLoginCheckBox = (CheckBox) findViewById(R.id.remember_me);
        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();
        saveLogin = loginPreferences.getBoolean("saveLogin", false);

        if (saveLogin) {
            user.setText(loginPreferences.getString("username", ""));
            pass.setText(loginPreferences.getString("password", ""));
            saveLoginCheckBox.setChecked(true);
        }

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ForgotPassword.class));
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            URL url;
            final HashMap<String, String> ConnDataParams = new HashMap<>();

            private boolean isNetworkAvailable() {
                ConnectivityManager connectivityManager
                        = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                return activeNetworkInfo != null && activeNetworkInfo.isConnected();
            }

            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()) {
                    Log.e("pressed","reached");
                    if (canClick) {
                        Log.e("clickable","reached");
                        canClick = false;
                        username = user.getText().toString();
                        password = pass.getText().toString();

                        ConnDataParams.put("email", username);
                        ConnDataParams.put("password", password);

                        if (saveLoginCheckBox.isChecked()) {
                            loginPrefsEditor.putBoolean("saveLogin", true);
                            loginPrefsEditor.putString("username", username);
                            loginPrefsEditor.putString("password", password);
                            loginPrefsEditor.commit();
                        } else {
                            loginPrefsEditor.clear();
                            loginPrefsEditor.commit();
                        }

                        try {
                            url = new URL(getResources().getString(R.string.LoginURL));
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }

                        ConnData = new Pair<>(url, ConnDataParams);
                        requestHandler = new RequestHandler(MainActivity.this, "MainActivity");
                        requestHandler.execute(ConnData);
                    }
                } else {
                    AlertDialog.Builder ErrorDlgAlert = new AlertDialog.Builder(MainActivity.this);
                    ErrorDlgAlert.setMessage("Check your internet connection");
                    ErrorDlgAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            canClick = true;
                        }
                    });
                    ErrorDlgAlert.setTitle("Error!");
                    ErrorDlgAlert.setCancelable(true);
                    ErrorDlgAlert.create().show();
                    canClick = true;
                }
            }
        });
    }
}