package com.example.eugenvlasie.sst;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import static com.example.eugenvlasie.sst.Dashboard.QR_BARCODE;
import static com.example.eugenvlasie.sst.NewProduct.BARCODE_REQUEST;
import static com.example.eugenvlasie.sst.NewProduct.REQUEST_CAMERA;

/**
 * Created by Eugen Vlasie on 06.07.2016.
 */
public class ManageProducts extends AppCompatActivity {
    private ProductListAdapter productListAdapter;
    static int refreshCode = 1;
    public static ArrayList<Product> products;
    private ProductDatabaseHandler productDatabase;

    ProgressDialog ringProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar_logo);
        getSupportActionBar().setElevation(0);

        ImageView backButton = (ImageView) findViewById(R.id.back_button);

        backButton.setOnClickListener(new View.OnClickListener() { //Functionality of Back Button in Action Bar
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        setContentView(R.layout.manage_products);
        Button qrScan = (Button) findViewById(R.id.qrscan);
        final EditText searchbar = (EditText) findViewById(R.id.searchbar);
        productDatabase = new ProductDatabaseHandler(ManageProducts.this, "ProductDatabase");


        final ListView menu = (ListView) findViewById(R.id.manage_products_menu);

        Log.e("products null & size",  " " + String.valueOf(products == null));

        if(products == null || products.size() == 0) {

            ringProgressDialog = ProgressDialog.show(this,"Please wait ...","Getting things ready ...",true);

            final Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    ringProgressDialog.dismiss();

                    productListAdapter = new ProductListAdapter(ManageProducts.this, products);
                    menu.setAdapter(productListAdapter);
                    menu.setTextFilterEnabled(true);
                    searchbar.setFocusable(true);
                }
            };

            new Thread(new Runnable() {
                @Override
                public void run() {
                    products = productDatabase.getProducts(0);
                    handler.sendEmptyMessage(0);
                }
            }).start();
        }
        else {
            productListAdapter = new ProductListAdapter(ManageProducts.this, products);
            menu.setAdapter(productListAdapter);
            menu.setTextFilterEnabled(true);
            searchbar.setFocusable(true);
        }
        searchbar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                ManageProducts.this.productListAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        menu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                ViewGroup itemView = (ViewGroup) view;

                TextView barcodeText = (TextView) itemView.getChildAt(1);
                String prodBarcode = (String) barcodeText.getText();
                int indexOfDivider = prodBarcode.indexOf(":");
                prodBarcode = prodBarcode.substring(indexOfDivider + 2);
                refreshCode = 1;
                Intent barcodeIntent = new Intent(ManageProducts.this, NewProduct.class);
                barcodeIntent.putExtra(QR_BARCODE, prodBarcode);
                ManageProducts.this.startActivity(barcodeIntent);
            }
        });

        qrScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(ManageProducts.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(ManageProducts.this, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
                } else {
                    refreshCode = 0;
                    Intent qrIntent = new Intent(ManageProducts.this, QRScanner.class);
                    qrIntent.putExtra("requestBarcode", BARCODE_REQUEST);
                    ManageProducts.this.startActivityForResult(qrIntent, BARCODE_REQUEST);
                }
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {

            case REQUEST_CAMERA: {
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        refreshCode = 0;
                        Intent qrIntent = new Intent(ManageProducts.this, QRScanner.class);
                        qrIntent.putExtra("requestBarcode", BARCODE_REQUEST);
                        ManageProducts.this.startActivityForResult(qrIntent, BARCODE_REQUEST);

                    } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {

                        if (ActivityCompat.shouldShowRequestPermissionRationale(ManageProducts.this, Manifest.permission.CAMERA)) {

                            Toast.makeText(ManageProducts.this, "This app requires this permission in order to acces your camera!", Toast.LENGTH_SHORT).show();

                        } else {

                            Toast.makeText(ManageProducts.this, "You have checked the \"Don't ask again\" box!", Toast.LENGTH_SHORT).show();

                        }
                    }
                }
            }
        }
    }

    @Override
    public void onRestart() {

        super.onRestart();

        if (refreshCode == 1) {

            Intent refresh = new Intent(ManageProducts.this, ManageProducts.class);
            overridePendingTransition(0, 0);
            refresh.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            finish();
            overridePendingTransition(0, 0);
            ManageProducts.this.startActivity(refresh);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == BARCODE_REQUEST) {

            if (resultCode == RESULT_OK) {
                String code = data.getStringExtra("barcode");
                Intent tempIntent = new Intent(ManageProducts.this, NewProduct.class);
                tempIntent.putExtra(QR_BARCODE, code);
                ManageProducts.this.startActivity(tempIntent);
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
