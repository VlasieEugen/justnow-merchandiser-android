package com.example.eugenvlasie.sst;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static com.example.eugenvlasie.sst.NewProduct.BARCODE_REQUEST;


public class QRScanner extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;
    static String barcodeFromScan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar_logo);
        getSupportActionBar().setElevation(0);

        ImageView backButton = (ImageView) findViewById(R.id.back_button);

        backButton.setOnClickListener(new View.OnClickListener() { //Functionality of Back Button in Action Bar
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mScannerView = new ZXingScannerView(QRScanner.this);
        setContentView(mScannerView);
        mScannerView.setResultHandler(QRScanner.this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }


    @Override
    public void handleResult(Result rawResult) {

        barcodeFromScan = rawResult.getText();
        Log.e("barcode raw",rawResult.getBarcodeFormat().toString());
        Log.e("barcode", barcodeFromScan);

        Intent req = getIntent();
        if (req.getIntExtra("requestBarcode", 0) == BARCODE_REQUEST) {
            Intent result = new Intent();
            result.putExtra("barcode", barcodeFromScan);
            setResult(Activity.RESULT_OK, result);
        }
        finish();
    }
}
