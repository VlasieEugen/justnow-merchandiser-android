package com.example.eugenvlasie.sst;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.net.URL;
import java.util.HashMap;

public class ForgotPassword extends Activity {

    private EditText mailAddress;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.forg_pass);
        Button changePassword = (Button) findViewById(R.id.send_mail);
        TextView backToLogin = (TextView) findViewById(R.id.backToLogin);

        backToLogin.setClickable(true);
        backToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mailAddress = (EditText) findViewById(R.id.e_mail);
        changePassword.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                URL url;
                final HashMap<String, String> ConnDataParams = new HashMap<>();
                email = mailAddress.getText().toString();

                try {

                    url = new URL(getResources().getString(R.string.ForgotPasswordURL));
                    ConnDataParams.put("email", email);

                    Pair<URL, HashMap<String, String>> ConnData = new Pair<>(url, ConnDataParams);
                    RequestHandler requestHandler = new RequestHandler(ForgotPassword.this, "ForgotPassword");
                    requestHandler.execute(ConnData);

                } catch (Exception e) {

                    e.printStackTrace();

                }

            }
        });

    }

}