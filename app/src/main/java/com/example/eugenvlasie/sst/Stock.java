package com.example.eugenvlasie.sst;


class Stock {
    private int id;
    private String units;
    private String expiration_date;
    private String created_at;
    private String updated_at;

    Stock(int id, String units, String expiration_date, String created_at, String updated_at) {
        this.id = id;
        this.units = units;
        this.expiration_date = expiration_date;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    Stock() {
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    String getUnits() {
        return units;
    }

    void setUnits(String units) {
        this.units = units;
    }

    String getExpiration_date() {
        return expiration_date;
    }

    void setExpiration_date(String expiration_date) {
        this.expiration_date = expiration_date;
    }

    String getCreated_at() {
        return created_at;
    }

    void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    String getUpdated_at() {
        return updated_at;
    }

    void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
