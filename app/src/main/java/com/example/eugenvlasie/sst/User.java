package com.example.eugenvlasie.sst;

class User {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    String getUsername() {
        return Username;
    }

    void setUsername(String username) {
        Username = username;
    }

    String getPassword() {
        return Password;
    }

    void setPassword(String password) {
        Password = password;
    }

    private int id;
    private String Username;
    private String token;

    String getToken() {
        return token;
    }

    void setToken(String token) {
        this.token = token;
    }

    private String Password;


    User(int id, String Username, String Password, String token) {
        this.id = id;
        this.token = token;
        this.Username = Username;
        this.Password = Password;
    }

    User() {

    }
}
