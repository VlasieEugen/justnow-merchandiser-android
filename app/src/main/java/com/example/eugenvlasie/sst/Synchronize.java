package com.example.eugenvlasie.sst;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static com.example.eugenvlasie.sst.MainActivity.token;

/**
 * Created by Eugen Vlasie on 01.07.2016.
 */

public class Synchronize extends Activity {

    static TextView tipConn;
    static TextView Sync;
    static ProgressBar progressBar;
    private RequestHandler requestHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        TextView Cancel;
        TextView checkConn;


        final HashMap<String, String> ConnDataParams = new HashMap<>();
        super.onCreate(savedInstanceState);


        setContentView(R.layout.synchronization_screen);

        checkConn = (TextView) findViewById(R.id.check_conn);
        tipConn = (TextView) findViewById(R.id.tip);

        if (isNetworkAvailable()) {
            checkConn.setText(getResources().getString(R.string.yes_conn));
        } else {
            checkConn.setText(getResources().getString(R.string.no_conn));
            tipConn.setText(getResources().getString(R.string.tip_conn));
        }

        Sync = (TextView) findViewById(R.id.synchronize);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        Cancel = (TextView) findViewById(R.id.cancel);
        Cancel.setClickable(true);
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (tipConn.getText().toString().equals(getResources().getString(R.string.sync_in_progress))) {
                    requestHandler.cancel(true);
                    ArrayList<Product> products;
                    ProductDatabaseHandler productDatabase = new ProductDatabaseHandler(Synchronize.this, "ProductDatabase");
                    ProductDatabaseHandler localProductDatabase = new ProductDatabaseHandler(Synchronize.this, "AuxiliaryProductDatabase");
                    if (requestHandler.isFinishedCopying()) {
                        productDatabase.onUpgrade(productDatabase.getWritableDatabase(), 1, 1);
                        products = localProductDatabase.getProducts();
                        for (Product k : products) {
                            productDatabase.addProduct(k);
                        }
                    }
                }*/
                if(requestHandler!=null)
                    requestHandler.cancel(true);
                //requestHandler.cancel(false);
                finish();
            }
        });
        Sync.setClickable(true);
        Sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isNetworkAvailable()) {

                    Sync.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    tipConn.setText(getResources().getString(R.string.sync_in_progress));
                }

                try {
                    URL url = new URL(getResources().getString(R.string.RefreshTokenURL));
                    ConnDataParams.put("token", token);
                    Pair<URL, HashMap<String, String>> ConnData = new Pair<>(url, ConnDataParams);
                    requestHandler = new RequestHandler(Synchronize.this, "Synchronize");
                    requestHandler.execute(ConnData);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
