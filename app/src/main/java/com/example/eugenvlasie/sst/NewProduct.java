package com.example.eugenvlasie.sst;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static com.example.eugenvlasie.sst.Dashboard.QR_BARCODE;
import static com.example.eugenvlasie.sst.ManageProducts.refreshCode;


public class NewProduct extends AppCompatActivity {

    private static final int SELECT_PICTURE = 1;
    static final int BARCODE_REQUEST = 2;
    private static final int TAKE_PICTURE = 3;
    private static final int REQUEST_READ_EXT_STORAGE = 4;
    static final int REQUEST_CAMERA = 5;
    private static int reqCamera;

    private Button library;
    private Button takePhoto;

    private EditText prodCode;
    private EditText prodName;
    private EditText sku;
    private EditText descrip;
    private EditText units;
    private Button exp_date;
    private EditText price;

    private ImageView productImage;

    private String prodImage;
    private ArrayList<Stock> stocks;
    private int prodId;
    private Timestamp prodCreatedDate;
    private String serverId;

    private int found = 0;

    private Product foundProduct;

    private Bitmap bitmap;

    private Uri photoURI;
    private File image;
    Bitmap imageBitmap;
    private boolean saved=false;

    private ProductDatabaseHandler productDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.e("TEXT: ", "onCreate");

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar_logo);
        getSupportActionBar().setElevation(0);

        ImageView backButton = (ImageView) findViewById(R.id.back_button);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        setContentView(R.layout.activity_new_product);

        getWindow().setBackgroundDrawableResource(R.drawable.bg);

        Intent qrIntent = getIntent();
        String barcodeQR = qrIntent.getStringExtra(QR_BARCODE);

        Button scan = (Button) findViewById(R.id.scan);
        library = (Button) findViewById(R.id.library);
        takePhoto = (Button) findViewById(R.id.take_photo);
        Button save = (Button) findViewById(R.id.save);
        TextView saveNew = (TextView) findViewById(R.id.save_add_new);

        prodCode = (EditText) findViewById(R.id.prod_code);
        prodName = (EditText) findViewById(R.id.prod_name);
        sku = (EditText) findViewById(R.id.sku);
        descrip = (EditText) findViewById(R.id.short_descrip);
        units = (EditText) findViewById(R.id.units);
        exp_date = (Button) findViewById(R.id.exp_date);
        price = (EditText) findViewById(R.id.price);
        productImage = (ImageView) findViewById(R.id.productImage);


        prodName.setEnabled(false);
        sku.setEnabled(false);
        descrip.setEnabled(false);
        units.setEnabled(false);
        price.setEnabled(false);
        exp_date.setEnabled(false);
        library.setEnabled(false);
        takePhoto.setEnabled(false);
        saveNew.setClickable(true);

        final ProgressDialog ringProgressDialog;
        productDatabase = new ProductDatabaseHandler(NewProduct.this, "ProductDatabase");


        if(ManageProducts.products == null) {

            ringProgressDialog = ProgressDialog.show(this,"Please wait ...","Getting things ready ...",true);

            final Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    ringProgressDialog.dismiss();
                }
            };

            new Thread(new Runnable() {
                @Override
                public void run() {
                    ManageProducts.products = productDatabase.getProducts(0);
                    handler.sendEmptyMessage(0);
                }
            }).start();
        }


        prodCode.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() >= 1) {
                    found = 0;
                    for (final Product q : ManageProducts.products) {
                        if (q.getBarcode().equals(s.toString()) && found == 0) {
                            found = 1;

                            prodName.setText(q.getProduct_name());
                            sku.setText(q.getSku());
                            descrip.setText(q.getShort_description());
                            units.setEnabled(true);

                            price.setText(q.getPrice());
                            price.setEnabled(true);

                            prodName.setEnabled(true);
                            sku.setEnabled(true);
                            descrip.setEnabled(true);
                            exp_date.setEnabled(true);
                            library.setEnabled(true);
                            takePhoto.setEnabled(true);
                            prodId = q.getId();

                            if(q.getStock().size() != 0) {
                                units.setText(q.getStock().get(q.getStock().size() - 1).getUnits());
                                exp_date.setText(q.getStock().get(q.getStock().size() - 1).getExpiration_date());
                                stocks = q.getStock();
                            }
                            price.setText(q.getPrice());

                            prodImage = NewProduct.this.getFilesDir() + "/" + prodCode.getText().toString() + ".jpg";
                            prodCreatedDate = q.getCreated_at();

                            serverId = q.getServerId();

                            File image = new File(prodImage);
                            if(image.exists()) {
                                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                                bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
                                //
                                productImage.getLayoutParams().height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());
                                productImage.requestLayout();
                                //
                                productImage.setImageBitmap(bitmap);
                            }
                            else {
                                Log.e("product image", " " + q.getProduct_image());

                                final Handler handler = new Handler() {
                                    @Override
                                    public void handleMessage(Message msg) {
                                        if (imageBitmap != null) {
                                            Log.e("inhandler imagebitmap", imageBitmap.toString());
                                            //
                                            productImage.getLayoutParams().height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());
                                            productImage.requestLayout();
                                            //
                                            productImage.setImageBitmap(imageBitmap);
                                        } else
                                            Toast.makeText(NewProduct.this, "Product's Image couldn't be loaded.", Toast.LENGTH_SHORT).show();
                                    }
                                };

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            InputStream in;
                                            if (!q.getProduct_image().contains("http://"))
                                                in = new java.net.URL("http://" + q.getProduct_image()).openStream();
                                            else
                                                in = new java.net.URL(q.getProduct_image()).openStream();
                                            Log.e("in thread", "yep");
                                            imageBitmap = BitmapFactory.decodeStream(in);
                                            handler.sendEmptyMessage(0);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                            Log.e("well, image not loaded", e.toString());
                                            handler.sendEmptyMessage(0);
                                        }
                                    }
                                }).start();
                            }



                            foundProduct = q;

                        }
                    }
                    if (found == 0) {

                        prodName.setEnabled(true);
                        sku.setEnabled(true);
                        descrip.setEnabled(true);
                        units.setEnabled(true);
                        price.setEnabled(true);
                        exp_date.setEnabled(true);
                        library.setEnabled(true);
                        takePhoto.setEnabled(true);

                        productImage.getLayoutParams().height=0;
                        productImage.setImageBitmap(null);

                        prodName.setText(null);
                        sku.setText(null);
                        descrip.setText(null);
                        units.setText(null);
                        price.setText(null);
                        prodImage = null;
                        prodCreatedDate = null;
                        prodId = 0;
                        exp_date.setText("yyyy-mm-dd");
                    }
                } else if (prodName.isEnabled()) {
                    prodName.setEnabled(false);
                    sku.setEnabled(false);
                    descrip.setEnabled(false);
                    units.setEnabled(false);
                    price.setEnabled(false);
                    exp_date.setEnabled(false);
                    library.setEnabled(false);
                    takePhoto.setEnabled(false);
                }
            }
        });

        if (barcodeQR != null) {
            prodCode.setText(barcodeQR);
            refreshCode = 1;
        }


        library.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prodImage = null;

                if (ContextCompat.checkSelfPermission(NewProduct.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        ActivityCompat.requestPermissions(NewProduct.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_READ_EXT_STORAGE);
                    }
                } else {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
                }
            }
        });

        takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prodImage = null;
                reqCamera = TAKE_PICTURE;

                if (ContextCompat.checkSelfPermission(NewProduct.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(NewProduct.this,Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(NewProduct.this,Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        ActivityCompat.requestPermissions(NewProduct.this, new String[]{Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA);
                    }
                } else {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    File storageDir = new File(Environment.getExternalStorageDirectory() + "/DCIM/Camera");
                    File phtoFile = new File(storageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
                    photoURI = Uri.fromFile(phtoFile);
                    saved=true;

                    takePicture.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePicture, TAKE_PICTURE);
                }
            }
        });

        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                reqCamera = BARCODE_REQUEST;

                if (ContextCompat.checkSelfPermission(NewProduct.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(NewProduct.this, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
                } else {
                    Intent intent = new Intent(NewProduct.this, QRScanner.class);
                    intent.putExtra("requestBarcode", BARCODE_REQUEST);

                    NewProduct.this.startActivityForResult(intent, BARCODE_REQUEST);
                }
            }

        });

        exp_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                final int cYear = c.get(Calendar.YEAR);
                final int cMonth = c.get(Calendar.MONTH);
                final int cDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePicker = new DatePickerDialog(NewProduct.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        month++;
                        if (month > 9) {
                            if (day > 9)
                                exp_date.setText(year + "-" + month + "-" + day);
                            else
                                exp_date.setText(year + "-" + month + "-0" + day);
                        } else {
                            if (day > 9)
                                exp_date.setText(year + "-0" + month + "-" + day);
                            else
                                exp_date.setText(year + "-0" + month + "-0" + day);
                        }

                    }
                }, cYear, cMonth, cDay);
                datePicker.show();

            }
        });

        saveNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateAddProduct() == 1) {
                    finish();
                    startActivity(new Intent(NewProduct.this, NewProduct.class));
                }
            }
        });

        save.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                if (validateAddProduct() == 1) {
                    finish();
                }
            }
        });

    }

    private int validateAddProduct() {
        int bug = 1;

        if (prodCode.getText().length() < 1 || (prodCode.getText().toString().charAt(0) < '0' || prodCode.getText().toString().charAt(0) > '9')) {
            Toast.makeText(NewProduct.this, "Please insert a valid barcode!", Toast.LENGTH_SHORT).show();
            bug = 0;
        }

        if ((prodName.getText().length() == 0 /*|| buggy == 0*/) && bug == 1) {
            Toast.makeText(NewProduct.this, "Please insert a valid Product Name!", Toast.LENGTH_SHORT).show();
            bug = 0;
        }


        if ((sku.getText().length() == 0 /*|| buggy == 0*/) && bug == 1) {
            Toast.makeText(NewProduct.this, "Please insert a valid SKU!", Toast.LENGTH_SHORT).show();
            bug = 0;
        }

        if ((descrip.getText().length() == 0 /*|| buggy == 0*/) && bug == 1) {
            Toast.makeText(NewProduct.this, "Please insert a valid Short description!", Toast.LENGTH_SHORT).show();
            bug = 0;
        }

        if ((units.getText().length() == 0) && bug == 1) {
            Toast.makeText(NewProduct.this, "Please insert a valid number of Units!", Toast.LENGTH_SHORT).show();
            bug = 0;
        }

        if ((price.getText().length() == 0) && bug == 1) {
            Toast.makeText(NewProduct.this, "Please insert a valid Price!", Toast.LENGTH_SHORT).show();
            bug = 0;
        }

        if (prodImage == null && bug == 1) {
            bug = 0;
            Toast.makeText(NewProduct.this, "Please insert an image for the product!", Toast.LENGTH_SHORT).show();
        }



        if (bug == 1) {
            Product product = new Product();
            if(saved)
                product.setProduct_image(prodImage);
            else {
                product.setProduct_image(prodImage);
                //product.setProduct_image(foundProduct.getProduct_image());
            }
            Log.e("product name",prodName.getText().toString());

            product.setProduct_name(prodName.getText().toString());
            product.setSku(sku.getText().toString());
            product.setBarcode(prodCode.getText().toString());
            product.setPrice(price.getText().toString());
            product.setServerId(serverId);

            product.setShort_description(descrip.getText().toString());

            Stock stock = new Stock();
            String tempExp = exp_date.getText().toString();
            String year = tempExp.substring(0, 4);
            String month = tempExp.substring(5, 7);
            String day = tempExp.substring(8);
            stock.setExpiration_date(year + "-" + month + "-" + day);

            stock.setUnits(units.getText().toString());
            String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
            stock.setUpdated_at(date);

            stock.setCreated_at(date);


            Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"), Locale.getDefault());
            String timeZone = new SimpleDateFormat("Z").format(calendar.getTime());

            String utcTime;
            if (timeZone.charAt(1) == '1') {
                utcTime = "UTC" + timeZone.substring(0, 3);
            } else {
                utcTime = "UTC" + timeZone.substring(0, 1) + timeZone.substring(2, 3);
            }


            if (found == 1) {

                Timestamp updatedAt = new Timestamp();
                updatedAt.setDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()));

                updatedAt.setTimezone(utcTime);
                product.setUpdated_at(updatedAt);

                product.setId(prodId);
                product.setCreated_at(prodCreatedDate);


            } else {
                Timestamp createdAt = new Timestamp();
                createdAt.setDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()));

                createdAt.setTimezone(utcTime);
                product.setCreated_at(createdAt);


                Timestamp updatedAt = new Timestamp();
                updatedAt.setDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()));
                updatedAt.setTimezone(utcTime);
                product.setUpdated_at(updatedAt);

                stocks = new ArrayList<>();
            }

            if (stocks == null) {
                stocks = new ArrayList<>();
            }

            stocks.add(stock);
            product.setStock(stocks);

            ProductDatabaseHandler db = new ProductDatabaseHandler(NewProduct.this, "ProductDatabase");

            if (found == 1) {
                Log.e("bef product size",ManageProducts.products.size()+"");
                ArrayList<Product> temp = new ArrayList<>();
                for(Product entity : ManageProducts.products) {
                    if(entity.getBarcode().equals(foundProduct.getBarcode())) {
                        temp.add(entity);
                        Log.e("removed", entity.getBarcode());
                    }
                }
                for(Product entity : temp)
                    ManageProducts.products.remove(entity);

                Log.e("after product size",ManageProducts.products.size()+"");
                ManageProducts.products.add(product);
                db.deleteProduct(foundProduct);
                db.addProduct(product,1);
                Toast.makeText(NewProduct.this, "Product has been successfully updated!", Toast.LENGTH_SHORT).show();
            } else {
                ManageProducts.products.add(product);
                db.addProduct(product,2);
                Toast.makeText(NewProduct.this, "Product has been successfully added!", Toast.LENGTH_SHORT).show();
            }
            if(bitmap != null) {
                ImageHandler imageHandler = new ImageHandler(prodCode.getText().toString() + ".jpg", bitmap, NewProduct.this);


                try {
                    imageHandler.saveImageToInternalStorage();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return 1;
        }
        return 0;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == BARCODE_REQUEST) {

            if (resultCode == RESULT_OK) {
                String code = data.getStringExtra("barcode");
                prodCode.setText(code);
            }
        } else if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK) {
            Uri selectedImageUri = data.getData();

            if (Build.VERSION.SDK_INT >= 19) {
                prodImage = getRealPathFromURI_API19(NewProduct.this, selectedImageUri);

                File image = new File(prodImage);
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);

            } else if (Build.VERSION.SDK_INT < 19) {
                prodImage = getRealPathFromURI_API11to18(NewProduct.this, selectedImageUri);

                File image = new File(prodImage);
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
           }


            if (prodImage != null) {
                Log.e("Gallery: ", prodImage);
                Toast.makeText(NewProduct.this, "The image has been loaded!", Toast.LENGTH_SHORT).show();
                bitmap = resizeAndRotate(bitmap, new File(prodImage));
                productImage.setImageBitmap(bitmap);
            } else
                Toast.makeText(NewProduct.this, "The image could not be loaded!", Toast.LENGTH_SHORT).show();
        } else if (requestCode == TAKE_PICTURE && resultCode == RESULT_OK) {
                if(photoURI!=null) {
                    prodImage = photoURI.toString().substring(7);
                }
                image = new File(prodImage);
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);

            if (prodImage != null) {
                Log.e("Camera APP: ", prodImage);
                Toast.makeText(NewProduct.this, "The image has been loaded!", Toast.LENGTH_SHORT).show();
                bitmap = resizeAndRotate(bitmap, new File(prodImage));
                productImage.setImageBitmap(bitmap);
            } else
                Toast.makeText(NewProduct.this, "The image could not be loaded!", Toast.LENGTH_SHORT).show();
        }
    }

    @TargetApi(19)
    private static String getRealPathFromURI_API19(Context context, Uri uri) {
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(uri);

        String id = wholeID.split(":")[1];

        String[] column = {MediaStore.Images.Media.DATA};

        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{id}, null);

        int columnIndex = cursor != null ? cursor.getColumnIndex(column[0]) : 0;

        if (cursor != null && cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        if (cursor != null) {
            cursor.close();
        }
        return filePath;
    }

    @TargetApi(11)
    private static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_EXT_STORAGE: {

                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);

                    } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {

                        if (ActivityCompat.shouldShowRequestPermissionRationale(NewProduct.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

                            Toast.makeText(NewProduct.this, "This app requires this permission in order to acces your gallery!", Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(NewProduct.this, "You have checked the \"Don't ask again\" box!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
            }
            case REQUEST_CAMERA: {
                if(grantResults.length > 1) {
                    int perm = 0;

                    if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        perm++;
                    }
                    else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {

                        if (ActivityCompat.shouldShowRequestPermissionRationale(NewProduct.this, Manifest.permission.CAMERA)) {

                            Toast.makeText(NewProduct.this, "This app requires this permission in order to acces your camera!", Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(NewProduct.this, "You have checked the \"Don't ask again\" box!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    if(grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                        perm++;
                    }
                    else if (grantResults[1] == PackageManager.PERMISSION_DENIED) {

                        if (ActivityCompat.shouldShowRequestPermissionRationale(NewProduct.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

                            Toast.makeText(NewProduct.this, "This app requires this permission in order to acces your gallery!", Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(NewProduct.this, "You have checked the \"Don't ask again\" box!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    if(grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                        perm++;
                    }
                    else if (grantResults[2] == PackageManager.PERMISSION_DENIED) {

                        if (ActivityCompat.shouldShowRequestPermissionRationale(NewProduct.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                            Toast.makeText(NewProduct.this, "This app requires this permission in order to acces your gallery!", Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(NewProduct.this, "You have checked the \"Don't ask again\" box!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    if(perm == 3) {
                        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                        File storageDir = new File(Environment.getExternalStorageDirectory() + "/DCIM/Camera");
                        File phtoFile = new File(storageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
                        photoURI = Uri.fromFile(phtoFile);

                        takePicture.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

                        startActivityForResult(takePicture, TAKE_PICTURE);
                    }
                }
                else if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                        if (reqCamera == TAKE_PICTURE) {

                            Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                            File storageDir = new File(Environment.getExternalStorageDirectory() + "/DCIM/Camera");
                            File phtoFile = new File(storageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
                            photoURI = Uri.fromFile(phtoFile);

                            takePicture.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

                            startActivityForResult(takePicture, TAKE_PICTURE);

                        } else if (reqCamera == BARCODE_REQUEST) {
                            Intent intent = new Intent(NewProduct.this, QRScanner.class);
                            intent.putExtra("requestBarcode", BARCODE_REQUEST);

                            NewProduct.this.startActivityForResult(intent, BARCODE_REQUEST);
                        }

                    } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {

                        if (ActivityCompat.shouldShowRequestPermissionRationale(NewProduct.this, Manifest.permission.CAMERA)) {

                            Toast.makeText(NewProduct.this, "This app requires this permission in order to acces your camera!", Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(NewProduct.this, "You have checked the \"Don't ask again\" box!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
            }
        }
    }

    private Bitmap resizeAndRotate(Bitmap bitmap, File imageFile) {

        int bmpWidth = bitmap.getWidth();
        int bmpHeight = bitmap.getHeight();

        float aspectRatio = bmpWidth / (float) bmpHeight;
        int width = 500;
        int height = Math.round(width / aspectRatio);
        bitmap = Bitmap.createScaledBitmap(bitmap, width, height, false);
        int newDimension = Math.min(bitmap.getWidth(), bitmap.getHeight());
        bitmap = ThumbnailUtils.extractThumbnail(bitmap, newDimension, newDimension);

        int rotate = 0;

        try {
            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("ou got problems","exif");
        }
        Matrix matrix = new Matrix();

        if (rotate == 90) {
            matrix.postRotate(90);
        }
        else if (rotate == 270) {
            matrix.postRotate(-90);
        }
        else if (rotate == 180) {
            matrix.postRotate(-180);
        }

        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        if(image != null)
            image.delete();

        return bitmap;
    }
}
