package com.example.eugenvlasie.sst;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

class ImageHandler {
    private String filename, url;
    private Bitmap bitmap;
    private int fromFile = 0;
    private Activity activity;

    ImageHandler(String filename, String url, Activity activity) {
        this.url = url;
        this.filename = filename;
        this.activity = activity;
        this.fromFile = 0;
    }

     ImageHandler(String filename, Bitmap bitmap, Activity activity) {
        this.activity = activity;
        this.bitmap = bitmap;
        this.filename = filename;
        this.fromFile = 1;
    }

    /*Bitmap getThumbnail() throws IOException {
        FileInputStream file = activity.openFileInput(filename);
        return BitmapFactory.decodeStream(file);
    }*/

    boolean saveImageToInternalStorage() throws IOException {

        final Bitmap[] btm = new Bitmap[1];
        if (fromFile == 0) {

            //Runnable r = new Runnable() {
  //              @Override
//                public void run() {
                    btm[0] = getBitmapFromURL(url);
                    //if (btm[0] == null)
                        //btm[0] = getBitmapFromURL("http://justnow.stonesoupdev.com/images/products/WaX42iVbMS.jpg");
    //            }
            //};

            //Thread thread = new Thread(r);
            //thread.start();
        } else {
            btm[0] = bitmap;
        }
        File oldImage=new File(activity.getFilesDir() + "/" + filename);
        oldImage.delete();
        FileOutputStream fos = activity.openFileOutput(filename, Context.MODE_PRIVATE);
        if(btm[0]!= null)
            btm[0].compress(Bitmap.CompressFormat.JPEG, 100, fos);
        fos.close();
        return true;
    }


    private Bitmap getBitmapFromURL(String src) {
        try {
            java.net.URL url = new java.net.URL(src);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();

            return BitmapFactory.decodeStream(input);

        } catch (IOException e) {
            e.printStackTrace();

            return null;
        }
    }

}

