package com.example.eugenvlasie.sst;

import android.util.Log;

import java.util.ArrayList;

import static com.example.eugenvlasie.sst.RequestHandler.shopId;

class Product {
    private int id;
    private String serverId;
    private ArrayList<Stock> stock;

    ArrayList<Stock> getStock() {

        return stock;
    }

    void addStock(Stock singleStock) {
        stock.add(singleStock);
    }

    void setStock(ArrayList<Stock> stock) {
        this.stock = stock;
    }

    private String product_name;
    private String sku;
    private String product_image;
    private String short_description;
    private String price;
    private String barcode;
    private String shop_id;
    private Timestamp created_at;
    private Timestamp updated_at;

    Timestamp getCreated_at() {
        return created_at;
    }

    void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    Timestamp getUpdated_at() {
        return updated_at;
    }

    void setUpdated_at(Timestamp updated_at) {
        this.updated_at = updated_at;
    }

    String getShop_id() {
        return shop_id;
    }

    void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    String getBarcode() {
        return barcode;
    }

    void setBarcode(String barcode) {
        this.barcode = barcode;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    String getProduct_name() {
        return product_name;
    }

    void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    String getSku() {
        return sku;
    }

    void setSku(String sku) {
        this.sku = sku;
    }

    String getProduct_image() {
        return product_image;
    }

    void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    String getShort_description() {
        return short_description;
    }

    void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    String getServerId() {
        return serverId;
    }

    void setServerId(String serverId) {
        this.serverId = serverId;
    }

    String getPrice() {
        return price;
    }

    void setPrice(String price) {
        this.price = price;
    }

    /*void printProduct() {
        Log.e("id", ((Integer) id).toString());
        Log.e("product_name", product_name);
        Log.e("sku", sku);
        Log.e("product_image", product_image);
        Log.e("short_description", short_description);
        Log.e("price", price);
        Log.e("barcode", barcode);
        Log.e("shop_id", shop_id);
        Log.e("Created at: ",created_at.getDate());
        Log.e("Updated at: ",updated_at.getDate());
        Log.e("Stocks", "Stocks:");
        for(Stock singleStock:stock) {
            Log.e("id", ((Integer) singleStock.getId()).toString());
            Log.e("Units", singleStock.getUnits());
            Log.e("created_at", singleStock.getCreated_at());
            Log.e("expiration_date", singleStock.getExpiration_date());
            Log.e("updated_at", singleStock.getUpdated_at());
            Log.e("Stock","End of stock");
        }
        Log.e("End of product", "Reached");
    }*/

    Product(int id, String product_name, String sku, String barcode, String price, String product_image, String short_description,
            String shop_id, ArrayList<Stock> stock, Timestamp created_at, Timestamp updated_at, String serverId) {
        this.id = id;
        this.serverId = serverId;
        this.product_name = product_name;
        this.sku = sku;
        this.product_image = product_image;
        this.short_description = short_description;
        this.price = price;
        this.barcode = barcode;
        this.stock = stock;
        this.shop_id = shop_id;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    Product() {
        this.shop_id=shopId;
    }
}
