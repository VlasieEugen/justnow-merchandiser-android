package com.example.eugenvlasie.sst;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;

import static com.example.eugenvlasie.sst.MainActivity.token;

public class SplashScreen extends Activity {
    static boolean canClick = true;
    static long timestamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.splash_screen);

        /*
        SharedPreferences.Editor editor = getSharedPreferences("timestamp", MODE_PRIVATE).edit();
        editor.putLong("seconds", 13);
        editor.commit();
         */

        SharedPreferences prefs = getSharedPreferences("timestamp", MODE_PRIVATE);
        timestamp=prefs.getLong("seconds",0);

        Log.e("timestamp",timestamp+"");
        canClick = true;
        super.onCreate(savedInstanceState);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                final AuthenticationDatabaseHandler db = new AuthenticationDatabaseHandler(SplashScreen.this);
                if (db.getUserCount() == 0) {
                    SplashScreen.this.startActivity(new Intent(SplashScreen.this, MainActivity.class));
                } else {
                    ArrayList<User> users = db.getAllUsers();
                    User user = users.get(0);
                    token = user.getToken();

                    Log.e("token", token);
                    SplashScreen.this.startActivity(new Intent(SplashScreen.this, Dashboard.class));
                }
            }
        }, 2000);
    }

    @Override
    public void onBackPressed() {
    }
}


