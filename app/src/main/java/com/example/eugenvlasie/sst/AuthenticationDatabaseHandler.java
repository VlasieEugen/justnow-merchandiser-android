package com.example.eugenvlasie.sst;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;


class AuthenticationDatabaseHandler extends SQLiteOpenHelper {

    AuthenticationDatabaseHandler(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    private static final int DATABASE_VERSION = 6;
    private static final String DATABASE_NAME = "DataBaseUsers";
    private static final String KEY_ID = "id";
    private static final String TABLE_USERS = "Users";
    private static final String USERNAME = "email";
    private static final String PASSWORD = "password";
    private static final String TOKEN = "token";

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String CREATE_USERS_TABLE = "CREATE TABLE " + TABLE_USERS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + USERNAME + " TEXT," + PASSWORD + " TEXT," + TOKEN + " TEXT" + ")";
        sqLiteDatabase.execSQL(CREATE_USERS_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        onCreate(sqLiteDatabase);

    }

    void addUser(User user) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(USERNAME, user.getUsername());
        values.put(PASSWORD, user.getPassword());
        values.put(TOKEN, user.getToken());

        db.insert(TABLE_USERS, null, values);
        db.close();

    }


    int getUserCount() {

        String countQuery = "SELECT  * FROM " + TABLE_USERS;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(countQuery, null);
        int x=cursor.getCount();
        cursor.close();
        return x;

    }

    void deleteUser(User user) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_USERS, KEY_ID + " =?",
                new String[]{String.valueOf(user.getId())});
        db.close();

    }


    ArrayList<User> getAllUsers() {

        ArrayList<User> Users = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_USERS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {

            do {

                User user = new User();
                user.setId(Integer.parseInt(cursor.getString(0)));
                user.setUsername(cursor.getString(1));
                user.setPassword(cursor.getString(2));
                user.setToken(cursor.getString(3));
                Users.add(user);

            } while (cursor.moveToNext());

        }
        cursor.close();

        return Users;

    }

}
